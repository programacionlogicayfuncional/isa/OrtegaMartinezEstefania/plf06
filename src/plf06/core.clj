(ns plf06.core
  (:gen-class))

(def mayusculas
  (hash-map \A 1 \Á 2 \B 3 \C 4 \D 5 \E 6 \É 7 \F 8 \G 9 \H 10 \I 11 \Í 12 \J 13 \K 14 \L 15 \M 16 \N 17 \Ñ 18 \O 19 \Ó 20 \P 21 \Q 22 \R 23 \S 24 \T 25 \U 26 \Ú 27 \Ü 28 \V 29 \W 30 \X 31 \Y 32 \Z 33))
  ;AÁBCDEÉFGHIÍJKLMNÑOÓPQRSTUÚÜVWXYZ
  ;33

(def minusculas
  (hash-map \a 1 \á 2 \b 3 \c 4 \d 5 \e 6 \é 7 \f 8 \g 9 \h 10 \i 11 \í 12 \j 13 \k 14 \l 15 \m 16 \n 17 \ñ 18 \o 19 \ó 20 \p 21 \q 22 \r 23 \s 24 \t 25 \u 26 \ú 27 \ü 28 \v 29 \w 30 \x 31 \y 32 \z 33))
  ;aábcdeéfghiíjklmnñóopqrstuúüvwxyz
  ;33

(def caracteres
  (hash-map \0 1 \1 2 \2 3 \3 4 \4 5 \! 6 \¡ 7 \# 8 \$ 9 \% 10 \& 11 \' 12 \( 13 \) 14 \* 15 \+ 16 \, 17 \- 18 \. 19 \/ 20 \: 21 \; 22 \< 23 \= 24 \> 25 \? 26 \@ 27 \[ 28 \´ 29 \] 30 \^ 31 \_ 32 \` 33 \{ 34 \| 35 \} 36 \~ 37 \5 38 \6 39 \7 40 \8 41 \9 42))
  ;01234!¡#$%&'()*+,-./:;<=>?@[´]^-`{|}~56789
  ;42

(def mayusculas-aux
  (vector \A \Á \B \C \D \E \É \F \G \H \I \Í \J \K \L \M \N \Ñ \O \Ó \P \Q \R \S \T \U \Ú \Ü \V \W \X \Y \Z))

(def minusculas-aux
  (vector \a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z))

(def caracteres-aux
  (vector \0 \1 \2 \3 \4 \! \¡ \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \´ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9))

(defn encriptacion
  [c]
  (letfn [(f [s]
            (if (empty? s)
              ""
              (if (contains? mayusculas (first s))
                (str (first (drop (mod (+ 12 (get mayusculas (first s))) 33) mayusculas-aux)) (f (rest s)))
                (if (contains? minusculas (first s))
                  (str (first (drop (mod (+ 12 (get minusculas (first s))) 33) minusculas-aux)) (f (rest s)))
                  (if (contains? caracteres (first s))
                    (str (first (drop (mod (+ 12 (get caracteres (first s))) 42) caracteres-aux)) (f (rest s)))
                    (str (first s) (f (rest s))))))))]
    (f c)))
;(str (first (drop (mod (+ 12 (get (hash-map mayusculas) (first c))) 33) mayusculas-aux)))

(encriptacion "Canción #72")
;(encriptacion "Canción" "#72")



(defn -main
  [& args]
  (if (empty? args)
    (println "Error.")
    (println (encriptacion (apply str args)))))



;lein run "Canción #72"
;lein run "Canción" "#72"
;lein run "Can" "ción" "#72"
;lein run "Can" "ción" "#" "72"